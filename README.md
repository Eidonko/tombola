# Command line tombola

Arriva natale, e si vuole giocare a tombola. Una soluzione semplice per estrarre i numeri in modo divertente
è data da questo piccolo software scritto con Awk e espeak. Awk dovrebbe essere disponibile su ogni
sistema Linux, mentre espeak va installato esplicitamente (su ubuntu, si fa `sudo apt-get install espeak`).

Lo script normalmente usa getline -- un comando che legge l'input da tastiera in modo bufferizzato. Significa che per leggere un carattere devi normalmente battere Invio. Esiste una alternativa: se compili `unbuf_get_single_char`, puoi usare la funzione `ugetchar()`, che non attende il tasto di Invio. Per compilare `unbuf_get_single_char`, esegui 

`gcc -o unbuf_get_single_char unbuf_get_single_char.c`

Per passare dal modo bufferizzato a quello non bufferizzato, edita la prima linea di `tombola` e setta `unbuffered=0` se vuoi il modo bufferizzato oppure `unbuffered=1` se vuoi il modo non bufferizzato. **Se usi il modo bufferizzato, non è necessario compilare `unbuf_get_single_char.c`**

A questo punto rendi `tombola` eseguibile attraverso il comando `chmod +x tombola`.

Una volta installato, il programma si invoca semplicemente con `./tombola`

Ecco come funziona:

[[_TOC_]]

## Comando "q"

Serve per abbandonare il gioco. Lo stato della tombola è salvato automaticamente nel file `SAVE`, e si può ricaricare con il comando "l" (vedi oltre).

## Comando "?"

<pre>
$ ./tombola
lo special è 71
Premi un tasto, dai
<b>?</b>
Command line tombola
Comandi: ENTER: estrai un numero
         l    : load: Carica lo stato
         r    : ripeti: Ripeti ultimo numero
         s    : save: Salva lo stato
         t    : tabellone: Stampa il tabellone
         u    : uscito? Uscito o no?
         ?    : Questo menu
Premi un tasto, dai
</pre>
*Premendo **?** seguito da Enter si stampa un breve riassunto dei comandi della tombola.*

## Comando Enter

Il comando più frequente è Enter, che estra un nuovo numero. Ad esempio:

<pre>
⏎
82: '‘A Tavula ‘mbandita,  La tavola imbandita'
Premi un tasto, dai
</pre>
*Premendo **⏎** (tasto Enter) si estrae un nuovo numero. Il numero viene letto assieme alla figura corrispondente nella Smorfia napoletana.*

## Comando "r"

Premendo **r** viene ripetuto l'ultimo numero. Il programma dice: *"Ho detto **n**"*, dove **n** è l'ultimo numero estratto.

## Comando "t"

Il tasto "**t**" stampa il tombolone. L'ultimo numero estratto ed il precedente sono evidenziati.

![Esemplificazione del comando **t**](t.png)

## Comando "u"

Il tasto "**u**" serve per chiedere al programma se un numero è già uscito. Il programma chiede quale numero si deve controllare e poi fornisce l'informazione richiesta.


## Comandi "s" e "l"

Sono rispettivamente il "save" e il "load" dello stato di gioco! 


## Lo "Special"

Sorpresina :smile:


## Cose da mettere a posto

Senz'altro la pronuncia della Smorfia :smile:

Ho da poco implementato un meccanismo di input non bufferizzato, per poter leggere i caratteri senza attendere la pressione del tasto di invio. Il meccanismo è da testare!!

E poi tante piccole cose... un po' per volta ^_^


<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#"><a rel="cc:attributionURL" property="dct:title" href="https://gitlab.com/Eidonko/tombola">Command line Tombola</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Eidonko">Eidon</a> is licensed under <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Attribution-NonCommercial-ShareAlike 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"></a></p> 
